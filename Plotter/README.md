Plotter takes input data and creates a Gnuplot generated PNG.

The first row is the titles for the lines.

The first column is used as the X axis.

GenSample.sh was used to create the sample.dat file.

Usage
```bash
cat sample.dat | ./plotter.pl
```

## Arguments
```
-t <text> : Set the title
-o <file name> : Out put to file, keep output
-c <next column header> : Each header will be assigned as a title for the columns in the order they are found.
-s <width> <height> : Set size width by height
-g : Generate only, do not open with viewer. Helpful when you need to generate a lot of graphs
-p : Save the plot file for manual adjustments.
-B : Use floating boxes plot style. Takes exactly three columns, int X axis, int range 1 and int range 2
-h = help
```

## Examples

```
cat sample.dat | ./plotter.pl
```

```
cat Floating_box_sample.dat | ./plotter.pl -B
```


#!/usr/bin/perl -w

# If one column, plot as Y or index X
# If two or more columns first column is X, the rest are a line graphed to Y

# examples:
# Network throughput
# ssh web11 "LANG=C sar -s 06:00:00 -e 12:00:00 -n DEV |  grep -E \"IFACE|bond0\" | gawk '{print \$1,\$3,\$4}'" | ./plotter.pl

# block device
# ssh spare "LANG=C sar -s 06:00:00 -e 12:00:00 -d | grep -E \"DEV|dev104-0\" | gawk '{print \$1,\$3,\$4,\$5,\$6}'"|./plotter.pl

use File::Temp qw/ :mktemp  /;
use POSIX;
use File::Copy;
my @Columns;
my @Headers;
my $Title;
my $MaxColumns = 0;
my $OutFile = "";
my $Width = 800;
my $Height = 600;
my $GenOnly = 0;
my $PlotSaver = 0;
my $Y2 = 0;
my $PlotStyle = "line";

sub Main {
	while ($#ARGV > -1) {
		my $Arg = shift @ARGV;
		if ($Arg eq "-t") { $Title = shift @ARGV; } 
		if ($Arg eq "-o") { $OutFile = shift @ARGV; }
		if ($Arg eq "-c") { 
			my $TempName = shift @ARGV;
			if($TempName =~ "(Y2)" ) {
				$Y2 = 1;
			}
			push(@Headers, $TempName); 
		}
		if ($Arg eq "-g") { $GenOnly = 1; }
		if ($Arg eq "-p") { $PlotSaver = 1; }		
		if ($Arg eq "-s") { $Width = shift @ARGV; $Height = shift @ARGV; }
		if ($Arg eq "-h") { ShowHelp(); }
		if ($Arg eq "-B") { $PlotStyle = "floatingBoxes"; }
	}
	# print "line 34\n";
	my @InLineHeaders; 
	my $FistLine = 1;
	my $UseHeaders = 0;
	while (<>) {
		my $Line = $_;
		$Line =~ s/^\s+//;
		$Line =~ s/\s+$//;
		if ($Line eq "" ) { 
			next;
		}

		# print ".";
		if($FistLine == 1) {
			@InLineHeaders = split(/\s+/, $Line);
			foreach (@InLineHeaders) {
				if( $_ =~ m/[a-zA-Z]/ ) {
					$UseHeaders = 1;
				}
			}
			$FistLine = 0;
			if($UseHeaders == 1) {
				# print "next index InLineHeaders = '".$#InLineHeaders."'\n";
				if($#InLineHeaders > 1) {
					# print "strip off first column header\n";
					shift @InLineHeaders;
				}
				my $NextIndex = 0;
				foreach (@InLineHeaders) {
					if(! defined($Headers[$NextIndex])) {
						print "set Headers[$NextIndex] = '$_'\n";
						$Headers[$NextIndex] = $_;
						if($_ =~ "(Y2)") {
							$Y2 = 1;
							print "set Y2 for $_\n";
						}
					}
					$NextIndex++;
				}
			}
			next;
		} else {
			if( $Line =~ m/[a-zA-Z]/ ) {
				print "Line: '$Line'\n";
				print "Yo, I told you, Can't plot this, Stop, Hammer time!\n";
				next;
			}			
		}
		my @TempColumns = split (/\s+/, $Line);
		if ( $#TempColumns >= $MaxColumns ) { 
			$MaxColumns = $#TempColumns + 1;
			#printf "new max cols = $MaxColumns\n";
		}
		push(@Columns, [ @TempColumns ]);
	}
	$TempBase = mktemp( "/tmp/PlotterTempXXXXX" );
	$Pfile = $TempBase.".plt";
	$Dfile = $TempBase.".dat";
	if($OutFile eq "") {
		$OutFile = $TempBase.".png";
	}
	print "Plot file='$Pfile'\n";
	print "DAT file='$Dfile'\n";
	print "OutFile = '$OutFile'\n";

	Plot();

	system("gnuplot", $Pfile);
	#system("eog", $OutFile);
	if ( $GenOnly == 0 ) {
		system("gthumb", $OutFile);
	}
	if ( $PlotSaver == 1 ) {
		move($Pfile, "./");
	}

	#unlink($Pfile, $Dfile);
	#if($OutFile eq $TempBase.".png") {
	#	unlink($OutFile);
	#}
}

sub ShowHelp {
	print" plotter.pl will take a stream of data piped in from another source and create a simple gnuplot.
If the input data is one column it will be the only plot
If the input data contains two or more columns the first column is ploted as the X axis and all other columns are a line on the graph.

-t <text> : Set the title
-o <file name> : Out put to file, keep output
-c <next column header> : Each header will be assigned as a title for the columns in the order they are found.
-s <width> <height> : Set size width by height
-g : Generate only, do not open with viewer. Helpful when you need to generate a lot of graphs
-p : Save the plot file for manual adjustments.
-B : Use floating boxes plot style. Takes exactly three columns, int X axis, int range 1 and int range 2
-h = help\n

Examples:
seq 15 | awk '{print \$1, sin(\$1), cos(\$1), sin(\$1%2)}' | ./plotter.pl  -t Tester -c SIN -c COS -c \"Saw tooth\"
seq 1 0.1 50 | awk '{print \$1, sin(\$1 + sin(\$1))}' | ./plotter.pl  -t \"tester\"
seq 1 0.1 50 | awk '{print \$1, sin(\$1 + cos(\$1))}' | ./plotter.pl  -t \"tester\"
\n";
	exit;
}

sub Plot {
	my $Samples = scalar(@Columns);
	my $XFormat = "unknown";
	open ($Pfh, ">", $Pfile) or die "can't open file";
	print $Pfh "set terminal png size $Width, $Height \n";
	if( $Columns[0]->[0] =~ m/\d\d:\d\d/ ) {
		$XFormat = "time";
		print "Looks like column 1 is time\n";
		print $Pfh "set timefmt \"%H:%M";
		if( $Columns[0]->[0] =~ m/\d\d:\d\d:\d\d/ ) {
			print $Pfh ":%S";
		} 
		print $Pfh "\"\nset xdata time\nset format x \"%H:%M\"\n";
	}
	if( $Columns[0]->[0] =~ m/\d\d\d\d-\d\d-\d\d/ ) {
		$XFormat = "date";
		print "Looks like column 1 is date\n";
		print $Pfh "set xdata time\n";
		print $Pfh "set timefmt '%Y-%m-%d'\n";
		print $Pfh "set format x '%Y-%m-%d'\n";
		printf $Pfh "set xrange ['%s':'%s']\n", $Columns[0]->[0], $Columns[@Columns - 1]->[0];
		print $Pfh "set xtics rotate by 270 offset 0, 0\n";
	}

	# TODO: set the range by reading the min and max values.
	print $Pfh "set yrange [0:*]\n";

	if($Y2 == 1) {
		print "Using Y2\n";

		# TODO: set the range by reading the min and max values.
		print $Pfh "set y2range [0:*]\nset y2tics\n";
	}
	if (defined($Title) ) { 
		print $Pfh "set title \"$Title\"\n";
	}
	print $Pfh "set output \"$OutFile\"\n";
	print $Pfh "set key left box\n";
	print $Pfh "set samples $Samples\n";
	my $TempTitle = "notitle";
	if (defined($Headers[0]) ) { 
		$TempTitle = "title '$Headers[0]'";
	}
	

	if ($PlotStyle eq "floatingBoxes") {
		print $Pfh "set style rect fc rgb 'orange' fs solid 0.5\n";
		print $Pfh "set xtics 1\n";
		my $Ci = 0;
		foreach ( 0 .. $#Columns ) {
			$Ci++;
			printf $Pfh "set obj rect from %01.2f, %d to %01.2f, %d\n", $Ci - 0.25, $Columns[$_]->[1], $Ci + 0.25, $Columns[$_]->[2];
		}
		print $Pfh "plot '$Dfile'";	
	} else {
		print $Pfh "plot '$Dfile'";
		print $Pfh " \\\n\t   using 1:2 $TempTitle with lines axis";
		if($Headers[0] =~ "(Y2)") {
			print $Pfh " x1y2 lw 2";
		} else {
			print $Pfh " x1y1";
		}

		for ( my $i = 3; $i < $MaxColumns+1; $i++ ) {
			if (defined($Headers[$i - 2]) ) { 
				$TempTitle = "title '$Headers[$i - 2]'";
			} 
			print $Pfh ", \\\n\t'' using 1:$i $TempTitle with lines axis";
			if($Headers[$i - 2] =~ "(Y2)") {
				print $Pfh " x1y2 lw 2";
			} else {
				print $Pfh " x1y1";
			}
		}
	}
	printf $Pfh "\n";
	close $Pfh;

	open ($Dfh, ">", $Dfile) or die "can't open file";	
	foreach ( 0 .. $#Columns ) {
		for ( my $i = 0; $i < $MaxColumns; $i++ ) {
			defined($Columns[$_]->[$i]) && print $Dfh "$Columns[$_]->[$i] ";
		}
		print $Dfh "\n";
	}
	close($Dfh);
}

Main();

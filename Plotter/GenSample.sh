#!/bin/bash

# This is to generate some sample data

echo "time thing.one thing.two(Y2)"
for i in {1..50};do
	T=$(date +%H:%M:00 -d"$i minutes")
	L1=$(echo "scale=1;(s($i)*10 + (2 * $i))" | bc -l | cut -d. -f1)
	L2=$(echo "scale=1;(c($i)*10 + (2 * $i) + 50)" | bc -l | cut -d. -f1)
	echo "$T $L1 $L2"; 
done
